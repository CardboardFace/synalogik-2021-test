# Synalogik Test
A Java API to read the contents of a plain text file displaying the total number of words, average word length, the most 
frequently occurring word's length and a list of the number of words of each length.

## Example
### Input
`Hello world & good morning. The date is 18/05/2016`

### Output
```
Word count = 9
Average word length = 4.556
Number of words of length 1 is 1
Number of words of length 2 is 1
Number of words of length 3 is 1
Number of words of length 4 is 2
Number of words of length 5 is 2
Number of words of length 7 is 1
Number of words of length 10 is 1
The most frequently occurring word length is 2, for word lengths of 4 & 5
```

# Installation
1. Install JDK11 and setup a `%JAVA_HOME%` environment variable.
2. Install Apache Maven and add its bin directory to your `PATH` environment variable.
3. (Re)open a terminal/cmd prompt window in this project's root folder.
4. Run `mvn clean install` to compile, test and package the JAR file.
5. Run `java -jar target/<jar-name>.jar` (find the jar and replace `<jar-name>` with the generated name).
6. Once running, my preferred method of calling the API is Postman:
![Example Postman call](readme/postman-preview.png)
   
## Postman settings
* Request type must be POST.
* Endpoint URL must be `localhost:8080/synalogik-service/processFile`.
* The body type must be set to `form-data`.
* Create a key-value pair called "file", with the type changed to `File`, then upload the file as the value.

# Features
## Assumptions on word definitions
* All whitespace is considered a separator between words.
* The only characters counted in a word are from the first to last alphanumeric characters.
  * This prevents words neighbouring sentence terminators, parentheses or quote marks having their length effected.
  * The spec considers `&` as a 1-letter word, therefore symbols that represent words are excluded from this rule when used without neighbouring other characters (I included `@`, as it can mean "each at").

### Examples
| Word | Length according to assumptions |
|------|---------------------------------|
| **Hello**, **world**. | 5, 5 |
| **Hello-world**? | 11 |
| £**3.00** | 4 |
| **The car was mauve/purple** | 3, 3, 3, 12 |
| (**dark** **green**) | 4, 5 |
| **Hello** "**world**", **goodbye** | 5, 5, 7 |
| **@ &** @**hello**& **sample@gmail.com**  | 1, 1, 5, 16 |

## Validation & Error Handling
### Spring
This API uses Spring's controller validation annotations to prevent a null file being posted to the server.

### Service
I created custom exceptions for some predictable error scenarios to inform the consumer of the issue in human-readable detail.

For instance, though it'd be unlikely to pass the controller-level validation, an empty file would throw an exception in the buffered reader, but would be handled using my FileFormatException, returning a helpful message and Bad Request HTTP code.

## Tests
### Running tests
Running `mvn test` or `mvn install` automatically runs:
* Jacoco coverage check to ensure all classes have test coverage (with appropriate exclusions specified in POM).
* SpotBugs checks.
* Checkstyle and Google-Style violations.
* All unit tests.

If any tests fail, `mvn install` will also fail.

### Testing coverage with Jacoco
Jacoco generates a unit-test coverage report when compiling within `target\site\jacoco\index.html`.
It also prevents builds when un-excluded classes have 0% test coverage.

# Contributing & IDE Setup
To setup an IDE for contributions, I recommend using IntelliJ.
These are the plugins I used during development:
* *SonarLint* - Detects and helps auto-fix or explain quality issues.
* *SpotBugs* - Finds bugs and potential vulnerabilities.
* *Lombok/Annotation Processing* - Reduce boilerplate code.
* *Checkstyle* - Enforces Google Checks for consistent coding standards.
* *Google-java-format* - Makes IntelliJ's code-formatter tool follow Google-style practises.
* *GoogleStyle Java code editor style* - Makes some reformat and checkstyle issues adhere to Google-Style.

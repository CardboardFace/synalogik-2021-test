package com.freddie.synalogik.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.freddie.synalogik.SynalogikApplication;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

@RunWith(SpringRunner.class)
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    classes = {SynalogikApplication.class},
    properties = {"application.properties"})
@ActiveProfiles("test")
@Slf4j
class TestFileProcessor {
  @Autowired private FileProcessor fileProcessor;
  private MultipartFile specExample;
  private MultipartFile bibleDailyExample;

  private MultipartFile createMultipartFile(String classPathLocation) throws IOException {
    Resource classPathResource = new ClassPathResource(classPathLocation);

    final byte[] content = classPathResource.getInputStream().readAllBytes();
    return new MockMultipartFile(
        Objects.requireNonNull(classPathResource.getFilename()),
        classPathResource.getFilename(),
        "text/plain",
        content);
  }

  private MultipartFile mockMultipartFile(String content) {
    return new MockMultipartFile(
        "mock.txt", "mock.txt", "text/plain", content.getBytes(StandardCharsets.UTF_8));
  }

  @BeforeEach
  void loadExamples() throws IOException {
    specExample = createMultipartFile("data/spec-example.txt");
    bibleDailyExample = createMultipartFile("data/bible_daily.txt");
  }

  @Test
  void testSpecExample() {
    String specResult = fileProcessor.processFile(specExample);

    assertEquals(
        String.format(
            "Word count = 9%n"
                + "Average word length = 4.556%n"
                + "Number of words of length 1 is 1%n"
                + "Number of words of length 2 is 1%n"
                + "Number of words of length 3 is 1%n"
                + "Number of words of length 4 is 2%n"
                + "Number of words of length 5 is 2%n"
                + "Number of words of length 7 is 1%n"
                + "Number of words of length 10 is 1%n"
                + "The most frequently occurring word length is 2, for word lengths of 4 & 5"),
        specResult);
  }

  @Test
  void testBibleDailyThrowsNoExceptions() {
    String result = fileProcessor.processFile(bibleDailyExample);

    // Log for manually reviewing output
    log.info(String.format("Bible processing finished with result:%n%s", result));

    assertTrue(true);
  }

  @Test
  void testBlacklistedSymbols() {
    String specResult = fileProcessor.processFile(mockMultipartFile("@ &"));

    assertEquals(
        String.format(
            "Word count = 2%n"
                + "Average word length = 1.000%n"
                + "Number of words of length 1 is 2%n"
                + "The most frequently occurring word length is 2, for word lengths of 1"),
        specResult);
  }

  @Test
  void testSymbolsExample() {
    String specResult = fileProcessor.processFile(mockMultipartFile("**** hello world! ****"));

    assertEquals(
        String.format(
            "Word count = 2%n"
                + "Average word length = 5.000%n"
                + "Number of words of length 5 is 2%n"
                + "The most frequently occurring word length is 2, for word lengths of 5"),
        specResult);
  }

  @Test
  void testIntegerListToSentence() {
    assertEquals("7", FileProcessor.integerListToSentence(Collections.singletonList(7)));
    assertEquals("11 & 32", FileProcessor.integerListToSentence(Arrays.asList(11, 32)));
    assertEquals("1, 3 & 10", FileProcessor.integerListToSentence(Arrays.asList(1, 3, 10)));
    assertEquals("2, 4, 5 & 12", FileProcessor.integerListToSentence(Arrays.asList(2, 4, 5, 12)));
  }
}

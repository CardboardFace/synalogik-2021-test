package com.freddie.synalogik.service;

import static org.junit.jupiter.api.Assertions.assertThrows;

import com.freddie.synalogik.SynalogikApplication;
import com.freddie.synalogik.exception.FileFormatException;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    classes = {SynalogikApplication.class},
    properties = {"application.properties"})
@ActiveProfiles("test")
class TestValidations {
  @Autowired private FileProcessor fileProcessor;

  @Test
  void testBlankFileValidation() {
    final MockMultipartFile invalidFile =
        new MockMultipartFile("hello.txt", "hello.txt", "text/plain", new byte[0]);
    assertThrows(FileFormatException.class, () -> fileProcessor.processFile(invalidFile));
  }
}

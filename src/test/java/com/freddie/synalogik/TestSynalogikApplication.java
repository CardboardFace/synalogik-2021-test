package com.freddie.synalogik;

import static org.junit.jupiter.api.Assertions.assertTrue;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    classes = {SynalogikApplication.class},
    properties = {"application.properties"})
@ActiveProfiles("test")
@Slf4j
class TestSynalogikApplication {

  @Test
  void contextLoads() {
    assertTrue(true);
  }
}

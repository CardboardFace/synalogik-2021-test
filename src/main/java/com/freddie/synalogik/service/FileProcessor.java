package com.freddie.synalogik.service;

import com.freddie.synalogik.exception.FileFormatException;
import com.freddie.synalogik.exception.FileProcessingException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/** Service responsible for processing incoming files and returning the word count descriptions. */
@Slf4j
@Service
public class FileProcessor {

  private static final List<String> BLACKLISTED_SYMBOLS = Arrays.asList("&", "@");

  /**
   * Formats a list of Integers into a comma & whitespace separated String, finishing with an
   * ampersand between the last 2 numbers. Made package-private for unit testing
   *
   * @param integers The numbers to format.
   * @return The formatted number list
   */
  static String integerListToSentence(List<Integer> integers) {
    StringBuilder formattedSentence = new StringBuilder();
    Iterator<Integer> iterator = integers.iterator();
    if (iterator.hasNext()) {
      formattedSentence.append(iterator.next());
    }
    while (iterator.hasNext()) {
      final Integer nextNumber = iterator.next();
      if (iterator.hasNext()) {
        formattedSentence.append(", ").append(nextNumber);
      } else {
        formattedSentence.append(" & ").append(nextNumber);
      }
    }
    return formattedSentence.toString();
  }

  private String buildSummary(
      int wordCount, double averageWordLength, HashMap<Integer, Integer> wordLengthAccumulators) {

    // Sort keys/word lengths into ordered list
    List<Integer> keys = new ArrayList<>(wordLengthAccumulators.keySet());
    Collections.sort(keys);

    // Loop through each key and build formatted String
    StringBuilder wordLengthAccumulatorLines = new StringBuilder();

    int mostFrequentLengthOccurrence = 0;
    List<Integer> mostOccurrencesWordLengths = new ArrayList<>();
    for (Integer key : keys) {
      final int occurrences = wordLengthAccumulators.get(key);
      wordLengthAccumulatorLines.append(
          String.format("Number of words of length %d is %d%n", key, occurrences));

      if (occurrences > mostFrequentLengthOccurrence) {
        mostFrequentLengthOccurrence = occurrences;
        mostOccurrencesWordLengths.clear(); // Wipe arraylist
      }

      if (occurrences == mostFrequentLengthOccurrence
          && !mostOccurrencesWordLengths.contains(key)) {
        mostOccurrencesWordLengths.add(key);
      }
    }

    String mostFrequentLengths = "";
    if (mostFrequentLengthOccurrence != 0) {
      mostFrequentLengths =
          ", for word lengths of " + integerListToSentence(mostOccurrencesWordLengths);
    }

    return String.format(
        "Word count = %d%n"
            + "Average word length = %1.3f%n" // Round to 3 decimal places
            + "%s"
            + "The most frequently occurring word length is %d%s",
        wordCount,
        averageWordLength,
        wordLengthAccumulatorLines,
        mostFrequentLengthOccurrence,
        mostFrequentLengths);
  }

  /**
   * Reads through a file, returning information about the words in the file.
   *
   * @param file The target file
   * @return The processing result description
   */
  public String processFile(MultipartFile file) {
    try (BufferedReader reader =
        new BufferedReader(new InputStreamReader(file.getInputStream(), StandardCharsets.UTF_8))) {
      if (!reader.ready()) {
        throw new FileFormatException("File has no contents");
      }

      HashMap<Integer, Integer> wordLengthAccumulators =
          new HashMap<>(); // Map representing word length (key) and occurrences (value)
      int wordCount = 0;
      int characterCount = 0;
      while (reader.ready()) {
        String line = reader.readLine();

        // Ignore line if it contains no characters
        if (line == null) {
          continue;
        }

        // Split on any whitespace (including tabs, as per spec)
        final String[] words = line.split("\\s");

        for (String word : words) {
          // Remove non-alphanumeric characters surrounding un-blacklisted word
          if (!BLACKLISTED_SYMBOLS.contains(word)) {
            word = word.replaceAll("(^\\W*)|(\\W*$)", "");
          }

          // Only count words that (still) contain characters
          if (!word.isEmpty()) {
            wordLengthAccumulators.merge(word.length(), 1, Integer::sum);
            wordCount += 1;
            characterCount += word.length();
          }
        }
      }

      // Avoid ever dividing by zero
      final double wordCountAverage;
      if (wordCount == 0) {
        wordCountAverage = 0;
      } else {
        wordCountAverage = characterCount / (double) wordCount;
      }

      return buildSummary(wordCount, wordCountAverage, wordLengthAccumulators);

    } catch (IOException ex) {
      throw new FileProcessingException("Error occurred when processing file", ex);
    }
  }
}

package com.freddie.synalogik.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/** Thrown when there are issues with the file itself, such as metadata, name or contents. */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class FileFormatException extends RuntimeException {
  public FileFormatException(String message) {
    super(message);
  }
}

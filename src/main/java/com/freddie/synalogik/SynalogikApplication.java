package com.freddie.synalogik;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Synalogik application test project.
 *
 * @author Freddie Chessell
 */
@SpringBootApplication
public class SynalogikApplication {

  /**
   * Runs the Spring Boot application.
   *
   * @param args command line arguments
   */
  public static void main(String[] args) {
    SpringApplication.run(SynalogikApplication.class, args);
  }
}

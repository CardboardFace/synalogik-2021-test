package com.freddie.synalogik.controller;

import com.freddie.synalogik.service.FileProcessor;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/** Handles incoming HTTP requests. */
@Validated
@RestController
public class FileController {
  @Autowired private FileProcessor fileProcessor;

  @PostMapping("/processFile")
  public ResponseEntity<String> processFile(@RequestParam("file") @NotNull MultipartFile file) {
    return new ResponseEntity<>(fileProcessor.processFile(file), HttpStatus.OK);
  }
}
